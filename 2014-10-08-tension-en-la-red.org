#+TITLE:       Tensión en la red - Libertad y control en la era digital
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2014-10-08 11:13
#+HTML_HEAD:   <meta property="og:title" content="Tensión en la red - Libertad y control en la era digital" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2014-10-08" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2014-10-08-tension-en-la-red.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com///www.estebanmagnani.com.ar/wp-content/uploads/2014/09/Tension-en-la-red-interior.pdf" />


Hoy, 8 de octubre, se presenta el libro *Tensión en la red* [fn:libro]
de *Esteban Magnani* [fn:esteban]. Es un libro *libre*, es decir que
se publica con una licencia *Creative Commons*, que permite
descargarlo en formato =epub= [fn:epub] o =pdf= [fn:pdf] y podés
fotocopiarlo o utilizar la obra a tu gusto mientras no sea con fines
comerciales y también se puede obtener en formato impreso gracias a
AUTORIA Consultora Editorial.

Aunque participé con algunos comentarios, sugerencias y mínimas
correcciones, da mucho placer volverlo a leer y redescubrir que mi
paranoia tiene pruebas, hace mucho tiempo que nos espían, no a mi o a
vos, sino que indiscrimidamente a todos y cada vez cuesta más tener
algo de anonimato y privacidad en la red de redes.

Los invito a todos a leer esta excelente obra que recopila muchísima
información sobre la lucha por la libertad y el control por medio de
la tecnología que nos rodea.

Gracias Esteban!

#+ATTR_HTML: :width 451 :height 605 :title Libertad y control en la era digital
[[http://www.estebanmagnani.com.ar/wp-content/uploads/2014/09/Tension-en-la-red-interior.pdf][file:img/tension-en-la-red.jpg]]

[fn:esteban] http://www.estebanmagnani.com.ar/quien-soy/
[fn:libro] http://www.estebanmagnani.com.ar/tension-en-la-red-el-libro/
[fn:pdf] http://www.estebanmagnani.com.ar/wp-content/uploads/2014/09/Tension-en-la-red-interior.pdf
[fn:epub] http://www.estebanmagnani.com.ar/wp-content/uploads/2014/09/Tensionenlared.epub

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a2abb3b333c91e0d3c15ea9e93a21589bd4d86f7][=2015-07-03 03:59=]] @ 01:00 hs - reemplazo :alt por :title y cambios menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5a0a2ff7ac725f2f34944ca43e72f9b521dcb102][=2014-10-12 12:27=]] @ 00:05 hs - nuevamente corrijo link epub tension en la red :-S
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/22852622feb54240abc66f12d9d3feeebd3ac640][=2014-10-10 11:16=]] @ 00:10 hs - agrego tension en la red
