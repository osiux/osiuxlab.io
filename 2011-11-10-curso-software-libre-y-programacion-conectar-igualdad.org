#+TITLE:       Curso básico para alumnos sobre software libre y programación
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2011-11-10 22:19
#+HTML_HEAD:   <meta property="og:title" content="Curso básico para alumnos sobre software libre y programación" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2011-11-10" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2011-11-10-curso-software-libre-y-programacion-conectar-igualdad.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


Miércoles 9 de noviembre de 2011

Conectar Igualdad invita a todos los alumnos de tercer a quinto año de
escuelas secundarias de gestión pública a participar del curso básico
gratuito sobre software libre y programación (GNU/Linux).

La capacitación será realizada en tres jornadas de 9 a 12: 30 horas, los
días jueves 24 y martes 29 de noviembre y el jueves 1º de diciembre en
el Microcine de Conectar Igualdad (Córdoba 1801).

Durante los encuentros, coordinados por Javier Castrillo -experto en el
área de Software Libre aplicado a la educación-, los asistentes podrán
introducirse al mundo de la programación con software libre y el
desarrollo de aplicaciones. El objetivo es comprender que utilizar
computadoras y modificar lo que hacen es más sencillo de lo que parece,
sólo se tienen que conocer las herramientas y conceptos básicos para
empezar.

En esta introducción al diseño de programas se utilizarán herramientas
como el sistema operativo Ubuntu, el intérprete de comandos Bash y el
lenguaje Python.

No se necesita experiencia previa en el manejo de computadoras y se
entregará todo el material de estudio, apuntes y herramientas de
software.

*El curso es libre y gratuito con cupo limitado por lo que los
inscriptos serán confirmados mediante un e-mail.*

Para anotarte, completar formulario en:

- http://www.conectarigualdad.gob.ar/sin-categoria/software-libre-y-programacion/



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
