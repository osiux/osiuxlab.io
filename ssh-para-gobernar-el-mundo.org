#+TITLE:       SSH para gobernar el mundo!
#+DESCRIPTION: Introducción a SSH
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    SysAdmin, Access, GNULinux, Remote, Security, SSH, Tutorial
#+DATE:        2023-05-20 11:05
#+HTML_HEAD:   <meta property="og:title" content="SSH para gobernar el mundo!" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2023-05-20" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/ssh-para-gobernar-el-mundo.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />

[[file:img/matrix-trinity-hack.png][file:tmb/matrix-trinity-hack.png]]

** ¿qué es =ssh= y para qué sirve?

   =ssh= permite la ejecución remota de un intérprete de comandos
   como =bash=

   - Más información en: https://es.wikipedia.org/wiki/Ssh

** ¿qué ventajas tiene? 

   - *Es seguro* porque cifra la conexión y nadie puede ver qué
     estamos haciendo
   - *Es rápido* porque sólo envía texto, por ello envía menos datos
     que si fuera una imagen de pantalla como programas como =vnc=
   - *Es versátil* porque permite interactuar con otros comandos
     obteniendo control total de un sistema de manera remota.

** ¿qué necesito para usar SSH?

   - /computadora remota/
     - *servidor SSH*
       - se instala haciendo:
	 #+BEGIN_EXAMPLE
	   sudo apt-get install openssh-server
	 #+END_EXAMPLE
     - *conexión a la red* (si es internet mejor!)
     - *puerto 22 abierto* (OjO con el firewall)
     - que la computadora esté encendida! (aunque más adelante
       veremos cómo encender remotamente una computadora)
   - /computadora local/
     - *cliente ssh*
       - en todos los linux *ya está instalado*!
       - en otros sistemas pod usar [[https://es.wikipedia.org/wiki/PuTTY][PuTTY]]
	 - https://es.wikipedia.org/wiki/PuTTY
     - *dirección IP* de computadora remota
     - *usuario válido* de computadora remota
     - *contraseña* del usuario de la computadora remota
     - que el *puerto 22 de salida esté habilitado* en el firewall
       (aunque hay trucos para saltearlo)

** ¿cómo me conecto por SSH?

   En una terminal hay que escribir el comando =ssh= luego el
   usuario remoto, por ejemplo: *pepe* y separado por un *@* arroba
   la dirección *IP* o el dominio (ej: =192.168.0.9= ó =osiux.com=)

   #+BEGIN_SRC sh
     ssh pepe@192.168.0.9
   #+END_SRC

** ¿qué sucede la primera vez?

   Siempre que nos conectamos por primera vez a un servidor SSH, nos
   aparece un mensaje para que confirmemos la autenticidad de la
   computadora a la que estamos conectándonos, por ahora somos
   confiados y le decimos que sí, tipeando *yes*

   #+BEGIN_EXAMPLE
     osiris@osiux.com:/home/osiris# ssh 192.168.0.9
     The authenticity of host 'lab2 (192.168.0.9)' can't be established.
     RSA key fingerprint is e1:92:a3:54:95:e6:67:b8:89:80:11:02:93:f4:05:36.
     Are you sure you want to continue connecting (yes/no)?      
   #+END_EXAMPLE

   - Las próximas veces este paso no es necesario.
   - Luego ingresamos la contraseña (password en inglés) y si es
     correcta, ingresamos al sistema remoto!

** Listo, entré a la matrix! ¿y ahora?

   Ahora podemos hacer todo lo que el usuario que usamos para
   conectarnos (en este caso *pepe*) pueda hacer!

   Ahora bien, resulta que este sistema remoto cuenta con un
   administrador, que ve que alguien que no es él, ingresó al
   sistema con su usuario! Entonces va a matar todos los procesos
   del usuario *pepe*, a fin de dejarnos afuera del sistema.

   Para esto, utiliza la herramienta =htop= que muestra todos los
   procesos y presionando la tecla =F4= nos permite filtrar por la
   palabra *pepe* y luego presionando =F9= y eligiendo =SIGKILL=
   termina cada uno de los procesos del usuario *pepe*.

   También se puede hacer desde la consola sin usar =htop=

   #+BEGIN_EXAMPLE
     root@lab2:/# pkill -u pepe
   #+END_EXAMPLE

** El intruso va por la revancha!

   Como lo dejamos afuera a *pepe* matando sus procesos, ahora está
   enojado! Entonces nos empieza a crear *carpetas* en
   el *Escritorio* de la siguiente manera:

   #+BEGIN_EXAMPLE
     pepe@lab2:~/$ cd Escritorio
     pepe@lab2:~/$ mkdir aguante
     pepe@lab2:~/$ mkdir la
     pepe@lab2:~/$ mkdir academia
   #+END_EXAMPLE

   Si sigue así en algún momento nos va a llenar el *Escritorio*

** Eliminando al intruso!

   Matar los procesos activos no es suficiente, debemos eliminar al
   usuario, para esto hacemos:

   #+BEGIN_EXAMPLE
     root@lab2:/# deluser pepe
   #+END_EXAMPLE

   - Listo! ahora *pepe* no puede ingresar a nuestro sistema

** El intruso es duro de domar!

   Resulta que *pepe* cuando ingresó la primera vez, fue astuto y
   cambió la contraseña de *root*. ¿Y cómo pudo hacerlo? es simple,
   el usuario *pepe* tenía permisos para usar *sudo* que permite
   convertirse en *root* (y ser el *DIOS* del sistema).
   Para cambiar la password hizo lo siguiente:

   #+BEGIN_EXAMPLE
     pepe@lab2:~/$ sudo -s
     [sudo] password for pepe: 
     root@lab2:/#

     root@lab2:/# passwd
     Cambiando la contraseña de root.
     Introduzca la nueva contraseña de UNIX: 
     Vuelva a escribir la nueva contraseña de UNIX: 
   #+END_EXAMPLE

** ¿Y ahora quién podrá defendernos?

   Si un intruso logró obtener cuenta *root*, estamos fritos! tiene
   completo control del sistema y sin que nos demos cuenta, salvo
   que haga algo muy evidente como borrar todo el disco, va a pasar
   desapercibido. Qué podemos hacer, para evitar esto?

   - Utilizar contraseñas fuertes, usando =md5= por ejemplo:
     #+BEGIN_EXAMPLE
       echo -n maradona | md5sum
       8b123b7a7cf86f5aa9424d1f379384d8
     #+END_EXAMPLE
   - Mucho más fácil de recordar, frases de paso:
     #+BEGIN_EXAMPLE
       Se te escapo la tortuga!
     #+END_EXAMPLE
   - NO usar la misma contraseña en todos lados!
   - NO usar *sudo*, loguearse como *root* sólo para tareas
     administrativas
   - NO permitir el login del usuario *root*
   - Sólo permitir login por /SSH/ desde algunas IPs

   - OjO: aunque quitando el cable de red, basta para desconectar a
     los intrusos, hoy día casi que vuelve inservible una compu
     desconectada del mundo!

** Controlando la matrix!

   Si bien usamos el ejemplo de un intruso, lo más probable es que
   nosotros seamos root en muchos otros sistemas, como la compu del
   trabajo, la de casa, la de un amigo/a, etc. Y poder administrar
   varios sistemas remotos usando /SSH/ u una sola terminal es
   posible.

   Para esto instalamos *Terminator*
   #+BEGIN_EXAMPLE
     sudo apt-get install terminator
   #+END_EXAMPLE

   Iniciamos *Terminator* y presionando *CTRL-SHIFT-E* y *CTRL-SHIFT-O*
   podemos dividir la pantalla vertical y horizontalmente tantas
   veces como sea necesario, obteniendo muchas terminales dentro de
   una sola y eligiendo la opción *retransmitir a todas* podemos
   tipear una sola vez y que ese comando vaya a todas las terminales
   conectadas! :-D

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/4e88e440a5fac7b1852d3aab42a7070e657aa556][=2023-05-20 11:43=]] agregar DESCRIPTION, KEYWORDS, OpenGraph e imagen en SSH para gobernar el mundo!
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
