#+TITLE:       generar /ChangeLog/ en =org-mode= usando =git log=
#+DESCRIPTION: Generar ChangeLog en org-mode usando git log
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    Blog, ChangeLog, Git, Log, Org, OrgMode
#+DATE:        2021-02-01 18:36
#+HTML_HEAD:   <meta property="og:title" content="generar ChangeLog en org-mode usando git log" />
#+HTML_HEAD:   <meta property="og:description" content="Generar ChangeLog en org-mode usando git log" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="" />
#+HTML_HEAD:   <meta property="og:article:author" content="" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com//mnt/data/git/osiux/osiux.gitlab.io/2021-02-01-git-log-org-changelog.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/git-bash-utils/git-log-org-changelog.png" />

[[file:img/git-bash-utils/git-log-org-changelog.png][file:tmb/git-bash-utils/git-log-org-changelog.png]]

** porqué registrar los cambios?

Estoy en otro intento por darle algo de continuidad a mi desantentido
/blog/ y para cambiar un poco la estrategia, ahora estoy comenzando a
escribir los /posts/ y publicarlos así como estén, llenos de errores e
inconclusos, la idea es ir mejorándolos poco a poco, y por esto es una
buena práctica ir registrando los cambios en un /ChangeLog/ para ver el
progreso, cómo mejoró y se fue corrigiendo.

** registrar los cambios manualmente es tedioso!

Algunos pocos /posts/, tenían un /ChangeLog/ manual y la verdad es que
es una tarea tediosa y propensa a errores, pero se puede automatizar,
ahora bien lo que más me interesaba era que cada entrada del /ChangeLog/
apunte al /commit/ del cambio a los efectos de visualizar fácilmente el
=diff=, pero esto conlleva un problema, el /hash/ del /commit/ del
cambio debe estar dentro del cambio y no es posible, a menos que en el
/subject/ del /commit/ escriba el mensaje pensando que va a ser una
línea del /ChangeLog/, y respetando el /commit/ atómico a un único
/post/ debería luego poder realizar un /commit/ con todos los /commits/
que conforman el /ChangeLog/.

** =git log= al rescate!

La ventaja de esta idea es que puedo crear un /script/ =bash= que reciba
como parámetro el nombre de archivo del /post/ e invoque a =git log=
para obtener el historial de cambios y construya el /ChangeLog/ y lo
inserte dentro del mismo archivo del /post/, de esta manera podría
generar /ChangeLog/ para cada archivo del /blog/ a futuro y del pasado.

** =git-log-org-changelog=

El /script/ inicial es bastante simple y por ahora asume que el título
=** ChangeLog= es la última sección de un archivo, lo busca, lo trunca,
genera el historial de cambios y lo concatena, obviamente en caso de no
existir, simplemente lo añade.

Para evitar incluir cambios recursivamente innecesarios, los /commits/
específicos de /ChangeLog/ irán con el /subject/ =ChangeLog:
filename.org=

** automatizando el /ChangeLog/

De momento =git-log-org-changelog= [fn:git-log-org-changelog] esta a
prueba, viendo que casos de usos se presentan para mejorarlo, pero muy
posiblemente lo voy a invocar desde del /hook/ =post-commit= para
automatizar por completo su generación totalmente desatendida.

** tal vez te interese leer

- [[file:2023-06-08-git-tag-summary-vs-git-tag-readme.org][=git-tag-summary= vs =git-tag-readme=]]
- [[file:2023-05-25-git-tag-changelog.org][=git-tag-changelog=]]
- [[file:2021-03-09-git-commiter-date.org][cambiar la fecha de un /commit/ usando =GIT_COMMITER_DATE=]]
- [[file:2021-02-03-git-post-commit-changelog.org][git =post-commit= /ChangeLog/]]

[fn:git-log-org-changelog] https://gitlab.com/osiux/git-bash-utils/-/raw/master/git-log-org-changelog



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/abb8f8cb6f7236f197d3ba30fea5236435793fd6][=2023-07-02 20:50=]] agregar imagen y links relacionados y actualizar OpenGraph en generar /ChangeLog/ en =org-mode= usando =git log=
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/4eed7b07cf0c7491780da7dc9c2402b0281dae3f][=2023-07-02 19:28=]] agregar DESCRIPTION, KEYWORDS, corregir sintaxis, agregar footnotes
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/1a143d693a8007793e04914f4e61a7abd3a7e083][=2021-02-04 19:50=]] corrijo link a script =git-log-org-changelog=
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/f99f1b8e4f3c561aad89598d5773b095cd342835][=2021-02-01 19:33=]] agrego generar /ChangeLog/ en =org-mode= usando =git log=
