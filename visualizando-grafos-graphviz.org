#+TITLE:       Visualizando Grafos usando Graphviz
#+DESCRIPTION: generación automática de grafos directamente desde una consola usando GrahpViz
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    GraphViz, circo, Diagrama, dot, fdp, Grafo, neato, Terminal, twopi
#+DATE:        2014-03-14 08:41
#+HTML_HEAD:   <meta property="og:title" content="Visualizando Grafos usando Graphviz" />
#+HTML_HEAD:   <meta property="og:description" content="generación automática de grafos directamente desde una consola" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2014-03-14" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/visualizando-grafos-graphviz.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/graphviz-dot.png" />


** Introducción

   Generación de distintos grafos como estructuras de datos, estructuras
   de árbol, diagramas entidad-relación, de redes, de flujo, etc,
   utilizando el conjunto de herramientas Graphviz y su integración con
   otras aplicaciones. Desarrollo de scripts para la generación
   automática de grafos directamente desde una consola.

** Qué es Graphviz?

   Es un conjunto de herramientas open-source realizado inicialmente en
   los laboratorios de investigación de AT&T para el dibujo de gráficos
   especificados en lenguaje de scripts DOT. Provee librerías para ser
   usadas por otras aplicaciones. Graphviz es software libre licenciado
   bajo CPL (Common Public License).

** Aplicaciones

   - Estructuras de datos.
   - Estructuras de árbol.
   - Representación de análisis social de redes.
   - Diagramas entidad relación.
   - Diagramas de redes.
   - Diagramas de flujo.
   - Diagramas de procesos.

** Grafos

   Un grafo es un conjunto de objetos llamados vértices (o nodos) y una
   selección de pares de vértices, llamados aristas (arcs en inglés) que
   pueden ser orientados o no. Típicamente, un grafo se representa
   mediante una serie de puntos (los vértices) conectados por líneas
   (las aristas).

** Generadores de gráficos

*** =dot= Gráficos direccionales

    #+BEGIN_SRC dot :exports code
      digraph dot
      {
        nodo1 -> nodo2;
        nodo2 -> nodo3;
        nodo2 -> nodo4;
        nodo1 -> nodo5;
        nodo5 -> nodo6;
        nodo5 -> nodo7;
        nodo1 -> nodo8;
      }
    #+END_SRC

    #+ATTR_HTML: :width 640 :height 412 :title GraphViz ejemplo usando dot
    [[file:img/graphviz-dot.png]]

*** =neato= Gráficos no-direccionales

    #+BEGIN_SRC dot :exports code
      graph neato
      {
        nodo1 -- nodo2;
        nodo2 -- nodo3;
        nodo2 -- nodo4;
        nodo1 -- nodo5;
        nodo5 -- nodo6;
        nodo5 -- nodo7;
        nodo1 -- nodo8;
      }
    #+END_SRC

    #+ATTR_HTML: :width 640 :height 443 :title GraphViz ejemplo usando neato
    [[file:img/graphviz-neato.png]]

*** =twopi= Gráficos radiales

    #+BEGIN_SRC dot :exports code
      graph twopi
      {
        nodo1 -- nodo2;
        nodo2 -- nodo3;
        nodo2 -- nodo4;
        nodo1 -- nodo5;
        nodo5 -- nodo6;
        nodo5 -- nodo7;
        nodo1 -- nodo8;
      }
    #+END_SRC

    #+ATTR_HTML: :width 640 :height 646 :title GraphViz ejemplo usando twopi
    [[file:img/graphviz-twopi.png]]

*** =fdp= Gráficos no-direccionales

    #+BEGIN_SRC dot :exports code
      graph fdp
      {
        nodo1 -- nodo2;
        nodo2 -- nodo3;
        nodo2 -- nodo4;
        nodo1 -- nodo5;
        nodo5 -- nodo6;
        nodo5 -- nodo7;
        nodo1 -- nodo8;
      }
    #+END_SRC

    #+ATTR_HTML: :width 640 :height 438 :title GraphViz ejemplo usando fdp
    [[file:img/graphviz-fdp.png]]

*** =sfdp= Gráficos no-direccionales

    #+BEGIN_SRC dot :exports code
      graph sfdp
      {
        nodo1 -- nodo2;
        nodo2 -- nodo3;
        nodo2 -- nodo4;
        nodo1 -- nodo5;
        nodo5 -- nodo6;
        nodo5 -- nodo7;
        nodo1 -- nodo8;
      }
    #+END_SRC

    #+ATTR_HTML: :width 640 :height 396 :title GraphViz ejemplo usando sdfp
    [[file:img/graphviz-sfdp.png]]

*** =circo= Gráficos circulares

    #+BEGIN_SRC dot :exports code
      graph circo
      {
        nodo1 -- nodo2;
        nodo2 -- nodo3;
        nodo2 -- nodo4;
        nodo1 -- nodo5;
        nodo5 -- nodo6;
        nodo5 -- nodo7;
        nodo1 -- nodo8;
      }
    #+END_SRC

    #+ATTR_HTML: :width 640 :height 600 :title GraphViz ejemplo usnado circo
    [[file:img/graphviz-circo.png]]

** Te recomiendo leer

- [[file:dot.org][OSiUX GraphViz]]
- [[file:2023-05-12-graphviz-git-flow-full-example-with-gitlab-mr.org][Graficar ejemplo completo de integrar GitFlow y GitLab MRs]]
- [[file:2023-03-06-how-to-automate-graphviz-generate-image-from-dot-file-using-alpine-org2blog.org][How To automate GraphViz generate image from dot file using alpine-org2blog]]
- [[file:2022-10-25-how-to-make-a-timeline-with-graphviz-using-timeline2dot.org][Cómo hacer una línea de tiempo con GraphViz]]
- [[file:2022-10-21-use-graphviz-for-slides.org][Usar Graphviz para generar Slides]]
