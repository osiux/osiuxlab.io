#+TITLE:       Las Cooperativas de tecnología argentinas contaron su experiencia de trabajo en el MIT
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiris@gcoop.coop
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2014-03-22 10:06
#+HTML_HEAD:   <meta property="og:title" content="Las Cooperativas de tecnología argentinas contaron su experiencia de trabajo en el MIT" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2014-03-22" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2014-03-22-facttic-mit-libre-planet.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/facttic-mit-libre-planet.jpg" />


#+ATTR_HTML: :width 400 :height 265 :title FACTTIC MIT LibrePlanet
[[file:img/facttic-mit-libre-planet.jpg]]

"Participar de *LibrePlanet* [fn:libreplanet] fue muy importante para
las cooperativas argentinas. La invitación fue para contar nuestro
caso y de esa manera, colaborar en el crecimiento de las cooperativas
de EEUU y del resto del mundo", aseguró /Leandro Monk/. El Libre
Planet se desarrolló, como desde hace varios años, en el
*Massachusetts Institute of Technology* (MIT) [fn:mit], uno de los
polos de la innovación tecnológica norteamericana ligada a la
industria del software y la informática. El tema central de este año
fue "Software Libre, Sociedades Libres", y se habló de la capacidad
del software libre para proteger a periodistas, activistas y usuarios
de la vigilancia de gobiernos y corporaciones. "La experiencia es
sumamente gratificante y forma parte de nuestro compromiso cooperativo
de fortalecer al movimiento", agregó.

"Nos invitan porque Argentina es el país con más cooperativas de
tecnología en el mundo, además de ser los únicos con una federación,
es decir, con un nivel avanzado de institucionalización. En ese
sentido, compartir con el movimiento cooperativo de EEUU fue muy
enriquecedor". En su charla, Leandro se refirió a las ventajas de
organizar los emprendimientos tecnológicos como cooperativas, ya que
"el capital necesario para el desarrollo del trabajo es
fundamentalmente el conocimiento de las personas que lo integran, sin
necesidad de grandes montos de inversión. Esto abre la posibilidad de
crear empresas cooperativas con muy bajo costo de inversión inicial en
activos fijos, a diferencia de otro tipo de cooperativas que precisan
de la compra de máquinas, herramientas y materias primas para empezar
a trabajar y desarrollar el producto", agregó. También destacó que
"los trabajadores especializados en información y tecnología están
acostumbrados a desarrollar su trabajo coordinando sus experiencias en
red y compartiendo información y conocimiento, especialmente los que
desarrollan tecnologías libres". Ejemplo de esto es el "kernel"
(núcleo) del sistema operativo libre Linux, "que cuenta con más de
quince millones de líneas de código aportadas por miles de
programadores alrededor del mundo".

También se refirió a la primera cooperativa argentina del rubro,
Tecso [fn:tecso], que se inicia en 2002 a partir de un grupo de
profesionales con trayectoria en importantes empresas que eligieron la
organización cooperativa por estar sustentada en la horizontalidad y
la participación, "para ellos, estas cualidades lo hacían un buen modo
de transitar la crisis económica y social que vivía el país", afirma
Leandro. En ese sentido, aseguró que el país ha vivido en los últimos
años "un proceso de creación de cooperativas de trabajo tecnológicas
que nos llevó a que tengamos hoy más de ellas que todo el resto del
mundo sumado". Actualmente funcionan en el país 23 cooperativas
repartidas en todo el país, que dan empleo a 300 especialistas y
trabajan mayoritariamente con software libre.

Por último, el presidente de *Facttic* [fn:facttic] hizo especial
hincapié en cómo "la tecnología es una expresión de la política; si
vos querés hacer una tecnología que centralice y acumule, hacés
Facebook. Nosotros pensamos en tecnologías distintas, que no
concentren, que distribuyan, que incluyan", resumió.

[fn:mit] http://web.mit.edu
[fn:libreplanet] http://libreplanet.org/2014
[fn:tecso] http://www.tecso.coop
[fn:facttic] http://facttic.org.ar

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a2abb3b333c91e0d3c15ea9e93a21589bd4d86f7][=2015-07-03 03:59=]] @ 01:00 hs - reemplazo :alt por :title y cambios menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/128e0973e1e213a5ff560b43f30e90fb8642c5f0][=2014-04-22 10:11=]] @ 00:21 hs - Agrego nota FACTTIC en el MIT
