#+TITLE:       AWX Ansible =tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables=
#+DESCRIPTION: Cómo corregir error "tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables"
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    Ansible, AWX, AnsibleTower, cli, error, config, cfg, automation, DevOps
#+DATE:        2019-04-10 17:46
#+HTML_HEAD:   <meta property="og:title" content="AWX Ansible =tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables=" />
#+HTML_HEAD:   <meta property="og:description" content="Cómo corregir error "tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables"" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/./2019-04-10-awx-ansible-tower-cli-failed-to-parse-some-of-the-extra-variables.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/awx-ansible-tower-cli-failed-to-parse-some-of-the-extra-variables.png" />

Estoy ejecutando un /playbook/ de /Ansible/ [fn:ansible] que utiliza
=tower-cli= [fn:awx-cli] para interactuar con un servidor *AWX* [fn:awx]
y me encontré varios minutos intentando corregir este error:

: TASK [debug] *******************************************************************************
: task path: /mnt/data/git/ansible-tower-cli/inventory.yml:10
: ok: [localhost] => {
:     "awx_config_file": "~/.tower_cli.cfg"
: }
: META: ran handlers
:
: TASK [CreateOrganization] ********************************************************************
: task path: /mnt/data/git/ansible-tower-cli/inventory.yml:16
: An exception occurred during task execution. To see the full traceback,
: use -vvv. The error was: verify_ssl=false fatal: [localhost]: FAILED!
: => {
: "changed": false,
: "module_stderr": "Traceback (most recent call last):
: File /tmp/ansible_aipolz/ansible_module_tower_organization.py,
: line 103, in <module> main()
: File /tmp/ansible_aipolz/ansible_module_tower_organization.py,
: line 84,
: in main tower_auth = tower_auth_config(module)
: File
: /tmp/ansible_aipolz/ansible_modlib.zip/ansible/module_utils/ansible_tower.py,
: line 57,
: in tower_auth_config
: File
: /usr/local/lib/python2.7/dist-packages/tower_cli/utils/parser.py,
: line 105,
: in string_to_dict 'variables.\variables: \%s' %var_string
: tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables.variables:
: [general]\nhost=http://192.168.1.100:80\npassword=XXX\nusername=admin\nverify_ssl=false\n\n",
: "module_stdout": "",
: "msg": "MODULE FAILURE",
: "rc": 1
: } to retry, use: --limit @/mnt/data/git/ansible-tower-cli/inventory.retry
:
: PLAY RECAP **********************************************************************************
: localhost                  : ok=1    changed=0    unreachable=0    failed=1

El problema es con el formato del archivo =~/.tower_cli.cfg= y es
necesario corregir lo siguiente:

- eliminar la definición de la sección =[general]=
- eliminar espacios en blanco entre =clave=valor=
- el parámetro =host= debe contener el protocolo (/http/ o /https/) y
  el número de puerto utilizado (80 o 443)

Ejemplo:

: # cat ~/.tower_cli.cfg
:   [general]              <--- ERROR delete line!
:   host=192.168.1.100     <--- ERROR add http://hostname:port
:   password = xxxxxxxxxxx <--- ERROR trim whitespaces!
:   username=admin
:   verify_ssl=false

Versiones:

: # ansible --version
: ansible 2.5.13
:   config file = None
:   configured module search path = [u'/home/osiris/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
:   ansible python module location = /usr/local/lib/python2.7/dist-packages/ansible
:   executable location = /usr/local/bin/ansible
:   python version = 2.7.13 (default, Sep 26 2018, 18:42:22) [GCC 6.3.0 20170516]
:
: # tower-cli version
: Tower CLI 3.3.3
: API v2
: AWX 2.1.0
: Ansible 2.7.1

[fn:awx]                  https://github.com/ansible/awx/
[fn:awx-cli]              https://github.com/ansible/tower-cli
[fn:ansible]              https://www.ansible.com/



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/168226b5e06490f8233d34f946e24edf08e794be][=2023-05-08 13:11=]] agregar DESCRIPTION, KEYWORDS, imagen y actualizar OpenGraph en /AWX Ansible =tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables=/
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/03db4fc70979978282d2b63d571d54922d492bb2][=2022-11-13 20:19=]] agregar y corregir footnotes en AWX Ansible =tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables=
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/642c747345c7b45e94e58dfe087d3cce78d604f7][=2019-04-16 00:36=]] Agregar .gitlab-ci.yml y publish.el para generar sitio en gitlab.com
