#+TITLE:       =lists.sh= es para amantes de las listas
#+DESCRIPTION: lists.sh es un MicroBlog para listas en texto plano
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    Blog, lists, txt, TextoPlano, minimimalist
#+DATE:        2022-07-20 23:47
#+HTML_HEAD:   <meta property="og:title" content="=lists.sh= es para amantes de las listas" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2022-07-20" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2022-07-20-lists.sh-es-para-amantes-de-las-listas.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />

[[file:img/osiux-links-sh.png][file:tmb/osiux-links-sh.png]]

** Hackers Minimalistas

Hace un par de días descubrí =prose.sh= [fn:prose-sh] que es una
/plataforma de blog para hackers/ pensada en la simplicidad y en lo
esencial, es decir el contenido en texto plano sin preocuparnos por el
formato visual y poniendo foco en la funcionalidad.

En cuestión de minutos, pude comprobar lo fácil que era empezar a
usarla y rápidamente migré mi /blog/ a =osiux.prose.sh= [fn:osiux-prose]

** todo puede ser una lista!

En la misma sintonía descubrí =lists.sh= [fn:lists-sh] que es un
/microblog para listas/, que no es mas que convertir cada línea de un
archivo =.txt= a un item de una lista, tan simple como suena, y los usos
pueden ser muy variados, desde /libros/, /series/, /películas/,
/frases/, etc, que alguien tiene ganas de compartir públicamente.

En mi caso, para empezar, se me ocurrió convertir mi listado de sitios
favoritos (que esta publicado en =osiux.com= [fn:osiux-links]) y
compartirlos en este nuevo formato en =osiux.lists.sh= [fn:osiux-lists]

Para esto adapté el /script/ =links2org= [fn:links2org] a
=links2lists.sh= [fn:links2lists] =:)=

Seguramente en el futuro encontraré alguna otra lista mas por publicar!

** también podés leer sobre

- [[file:2022-07-17-prose.sh-a-blog-platform-for-hackers.org][=prose.sh= a blog platform for hackers]]
- [[file:2021-01-29-bookmarks-vs-links.org][bookmarks vs links.txt]]
- [[file:2021-04-28-gemini-es-para-amantes-del-texto-plano.org][=gemini://= es para amantes del texto plano]]
- [[file:todo-txt-rst+org-mode.org][TODO TXT | rst + org-mode]]

[fn:prose-sh]     https://prose.sh/
[fn:osiux-prose]  https://osiux.prose.sh/
[fn:osiux-links]  https://osiux.com/links.html
[fn:osiux-lists]  https://osiux.lists.sh/
[fn:lists-sh]     https://lists.sh/
[fn:links2org]    https://gitlab.com/osiux/links-bash-utils/-/raw/master/links2org
[fn:links2lists]  https://gitlab.com/osiux/links-bash-utils/-/raw/master/links2lists.sh


** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/49295fc6cab79a36dc24a673f97329ac96569115][=2022-07-21 00:12=]] agregar =lists.sh= es para amantes de las listas
