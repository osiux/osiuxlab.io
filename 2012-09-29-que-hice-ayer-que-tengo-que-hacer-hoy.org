#+TITLE:       ¿Que hice ayer? ¿Que tengo que hacer hoy?
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2012-09-29 22:19
#+HTML_HEAD:   <meta property="og:title" content="¿Que hice ayer? ¿Que tengo que hacer hoy?" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2012-09-29" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-09-29-que-hice-ayer-que-tengo-que-hacer-hoy.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


Hoy participé del
[[http://in.cafelug.org.ar/2012/08/2-programa-de-charlas---2012/index.html][CaFeIN]]
dando una charla sobre el uso de [[http://orgmode.org][Org-mode]] en el
día a día de un desarrollador (o al menos =BUGFIXER=).

Tuve suerte y logré dar casi todo el temario planteado, lo mejor fué capturar
la pantalla en un video para luego unirlo a la filmación de la charla, de esta
manera ahora puedo ofrecer un video compuesto que puede ser de gran ayuda para
todo aquél que tenga ganas de aprender más sobre *Org mode*

En algún momento lo mejoraré, queda en el =TODO=, acepto críticas, dudas y
comentarios al respecto!

- Descargar Video [[http://pub.osiux.com/charlas/CaFeIN-que-hice-ayer-que-tengo-que-hacer-hoy.ogv]]

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/00977674c60135495c431e80cae66c249d08aa42][=2012-12-16 09:50=]] @ 00:43 hs - Corrijo formato archivos.
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
