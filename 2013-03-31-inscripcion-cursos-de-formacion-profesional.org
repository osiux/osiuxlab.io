#+TITLE:       Inscripción Cursos de Formación Profesional en Software Libre
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2013-03-31 20:09
#+HTML_HEAD:   <meta property="og:title" content="Inscripción Cursos de Formación Profesional en Software Libre" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2013-03-31" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2013-03-31-inscripcion-cursos-de-formacion-profesional.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


[[http://gcoop.coop][gcoop]] abrió la inscripción a los *Cursos de Formación Profesional en
Software Libre*, en convenio con el /Ministerio de Trabajo de la
Nación/. Este año se abre el Nivel II, para todos aquellos que ya
tienen un conocimiento en el uso de la computadora y el trabajo con
GNU/Linux.

Los invitamos a inscribirse con el siguiente formulario:

- http://www.gcoop.coop/formulario-de-inscripcion-cursos-de-formacion-continua

- Las vacantes son limitadas
- La cursada es presencial y obligatoria dado que se entregan
  certificados oficiales.

Los cursos se desarrollan en el /Centro Político y Cultural Nuevo
Encuentro Comuna 14/, ubicado en *Gurruchaga 2122*.

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/42c841f182823fee0a7eb8647a1dab38aab95b43][=2013-03-31 20:34=]] 00:10 hs - Agrego inscripción Cursos de Formación Profesional en Software Libre.
