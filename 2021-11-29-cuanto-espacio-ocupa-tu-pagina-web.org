#+TITLE:       Cuánto espacio ocupa tu página web?
#+DESCRIPTION: Cuánto espacio ocupa tu página web? Sumate al club 512KB
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    Blog, Club512KB, minimalism, static, web
#+DATE:        2021-11-29 11:54
#+HTML_HEAD:   <meta property="og:title" content="Cuánto espacio ocupa tu página web?" />
#+HTML_HEAD:   <meta property="og:description" content="Cuánto espacio ocupa tu página web? Sumate al club 512KB" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2021-11-29" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2021-11-29-cuanto-espacio-ocupa-tu-pagina-web.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/osiux-com-the512kb-club-the-green-team.png" />

[[file:img/osiux-com-the512kb-club-the-green-team.png][file:tmb/osiux-com-the512kb-club-the-green-team.png]]

** purista del minimalismo

Desde el comienzo de este /blog/ tuve claro que NO usaría /Javascript/ y
que además esencialmente mantendría un repositorio de texto plano que
luego se convertiría a /HTML/, al comienzo nació en formato
**reSTructuredtext** [fn:rst] y luego migré a **org-mode** [fn:org-mode]
, ya que de esta manera garantizaba que el sitio pueda mantenerse en el
tiempo fácilmente y ser leído desde un directorio en el /filesystem/, de
un repositorio /git/ [fn:osiux-git], desde un navegador web de consola o
de un viejo y deprecado /browser/ sin muchos requerimientos, lo
importante debería ser el contenido de los /posts/ más que su formato.

** siempre se puede optimizar un poco más

**GTMetrix** [fn:gtmetrix] es una excelente herramienta para optimizar
un sitio web, sugiere exactamente que cambios son necesarios realizar
para que tu sitio web responda más rápido y es vital hacerlo si querés
tener muchas visitas, cuantos menos /bytes/ sean necesarios transferir y
menor sea la latencia, mejor será la experiencia de quien visite tu
sitio, sobre todo en lugares y/o momentos donde la conexión es malísima
y cada /byte/ cuenta.

** =512KB= son más que suficientes!

**512KB.club** [fn:512kb] es una colección de sitios web enfocados en la
performance y todos ocupan menos de =512KB=, dividido en tres categorías
según la cantidad de /bytes/ sin comprimir que ocupan las páginas:

- [[https://512kb.club/#100][/The Green Team/ =(<100KB)=]]
- [[https://512kb.club/#250][/Orange Team/ =(<250KB)=]]
- [[https://512kb.club/#512][/Blue Team/ =(<512KB)=]]

Orgullosamente estoy en =The Green Team=, pero lo más interesante de
unirme a =512KB Club= fue contar con un listado muy interesante de
sitios a descubrir donde gran parte de ellos tienen una creatividad
inusual en cómo se presentan al mundo llevando el minimalismo al
extremo! Recomiendo visitar estos sitios para sorprenderte gratamente.

[fn:osiux-git] https://gitlab.com/osiux/osiux.gitlab.io/
[fn:gtmetrix]  https://gtmetrix.com/
[fn:512kb]     https://512kb.club/
[fn:org-mode]  https://orgmode.org/
[fn:rst]       file:restructuredtext-texto-re-estructurado.org



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/eab47336bd84686b187b325a94685e3455a706d5][=2023-07-16 23:12=]] agregar DESCRIPTION, KEYWORDS y actualizar OpenGraph en /Cuánto espacio ocupa tu página web?/
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a774aeb120c160a11c22b3abcf807bc4cbf45a4b][=2021-11-29 18:10=]] agregar /Cuánto espacio ocupa tu página web?/
