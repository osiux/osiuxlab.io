#+TITLE:       CISL Conferencia Internacional Software Libre
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2012-10-15 22:19
#+HTML_HEAD:   <meta property="og:title" content="CISL Conferencia Internacional Software Libre" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2012-10-15" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-10-15-cisl-conferencia-internacional-software-libre.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/redes-para-todos.png" />


Hoy comenzó el [[http://cisl.org.ar][CISL]] y
voy a estar hablando sobre Redes Libres,
el miércoles 17 a la tarde

#+BEGIN_EXAMPLE
  Charla: REDES PARA TODOS
  Cuándo: MIERCOLES 17/OCTUBRE de 14:00 a 14:50
  Dónde:  Biblioteca Nacional, Agüero 2502 CABA
          Sala Augusto Cortazar
#+END_EXAMPLE

La Conferencia Internacional de Software Libre
está plagada de charlas y talleres muy buenos!

*¡NO SE LA PIERDAN! - Entrada Libre y Gratuita!*

[[file:img/redes-para-todos.png]]

[[http://pub.osiux.com/redes-para-todos.pdf]]


** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
