#+TITLE:       Ideas para utilizar =txt-bash-jrnl= como un sistema de micro blog
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiris@gcoop.coop
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2017-07-07 03:43
#+HTML_HEAD:   <meta property="og:title" content="Ideas para utilizar =txt-bash-jrnl= como un sistema de micro blog" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2017-07-07" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2017-07-07-ideas-para-utilizar-txt-bash-jrnl-como-un-sistema-de-micro-blog.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


-   leer archivo =.txt= y separar *header* de *body*.

-   identificar =.txt= como *Markdown*, *reStructuredtext* u otro formato
    para aplicar el parser/conversor adecuado a =.html=

-   el *header*, puede contener la metadata básica, comenzando por la
    fecha y hora de creación, un *issue* relacionado o un *tag* (a definir
    formato)

-   es posible convertir un archivo a
    http://localhost/YYYY-MM-DD/title-of-jrnl-blog.html

-   ver si es posible integrarlo a *Materialize*, hay una *PoC* que hice
    hace un tiempo en un *script* llamado =materialize.sh=

-   analizar soporte de sintaxis *Markdown* vs *reStructuredtext* y ver si
    es posible identificarlos mediante el comando =file=, tal vez sea más
    simple definir un tipo de sintaxis en el *header* y otro para el
    *body* ya que actualmente el header de =jrnl= esta pensado para
    comentarios de *Redmine*. Ver los siguientes links:

    - https://gist.github.com/dupuy/1855764
    - https://www.jungledisk.com/blog/2017/01/20/restructuredtext-vs-markdown/
    - https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
    - http://eli.thegreenplace.net/2017/restructuredtext-vs-markdown-for-technical-documentation/
    - http://www.cirosantilli.com/markdown-style-guide/#lists

-   ver de pulir la integración entre *Vim*, *Git* y *Redmine*.

** Esquema del archivo =.txt=

   : 
   :                          +<----- PARAGRAPH .md/.rst/.etc
   :                          |
   :                          |  +<-- <pre>ident text</pre>
   :                          |  |
   :             header     -> YYYY-MM-DD HH:MM [#1234] title of jrnl/blog
   :             blank line ->
   :         +--            -> short paragraph
   :         |   blank line ->
   :         |              ->   # comand one
   :         |              ->   line 1 result of comand one
   :         |              ->   line 2 result of comand one
   :  body --+   blank line ->
   :         |              ->   # comand two
   :         |              ->   line 1 result of comand two
   :         |              ->   line 2 result of comand two
   :         |   blank line ->
   :         |              -> long text paragraph
   :         |   blank line ->
   :         +-- spent time ->   @ HH:MM hs
   :                             |
   :                             +<-- <pre>indent text</pre>
   : 

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/882b1cda084f7c18362c8414a78dd65c7b968a3f][=2019-04-09 03:01=]] Recuperar archivos en .md y convertirlos a .org
