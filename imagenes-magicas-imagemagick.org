#+TITLE:       imágenes mágicas | imagemagick
#+DESCRIPTION: Creación y edición de imágenes sin usar un editor gráfico, directamente desde una consola.
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    Blog, Consola, Design, ImageMagick, Terminal, tty
#+DATE:        2010-05-10 15:30
#+HTML_HEAD:   <meta property="og:title" content="imágenes mágicas | imagemagick" />
#+HTML_HEAD:   <meta property="og:description" content="Creación y edición de imágenes sin usar un editor gráfico, directamente desde una consola." />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2010-05-10" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/imagenes-magicas-imagemagick.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/osiux-image-magick.jpg" />


** Descripción breve

Resolución a problemas comunes en la manipulación de imágenes mediante
la generación de _scripts_ usando =imagemagick= y otras utilidades.

** Crear Imagen

#+BEGIN_SRC sh :session :results none :exports code
  convert -size 1920x1080 xc:green img/green.png
#+END_SRC

[[file:img/green.png][file:tmb/green.png]]

** identify

#+BEGIN_SRC sh :session :results output :exports code
  identify img/green.png
#+END_SRC

#+BEGIN_EXAMPLE
  img/green.png PNG 1920x1080 1920x1080+0+0 8-bit PseudoClass 1c 252B 0.000u 0:00.000
#+END_EXAMPLE

** Creative Commons

#+BEGIN_SRC sh :session :results none :exports code
  convert img/green.png -fill yellow \
  -font ~/.fonts/cc-icons.ttf \
  -pointsize 32 -gravity SouthEast \
  -annotate 0 'cba ' \
  img/green-cc-by.jpg
#+END_SRC

[[file:img/green-cc-by.jpg][file:tmb/green-cc-by.jpg]]

** gradient

#+BEGIN_SRC sh :session :results none :exports code
  convert -size 1920x1080 gradient:none-gray img/gradient.png
#+END_SRC

[[file:img/gradient.png][file:tmb/gradient.png]]

** dark

#+BEGIN_SRC sh :session :results none :exports code
  composite -compose colorburn img/gradient.png \
	img/green-cc-by.jpg img/dark-green.jpg
#+END_SRC

[[file:img/dark-green.jpg][file:tmb/dark-green.jpg]]

** font

#+BEGIN_SRC sh :session :results none :exports code
  convert img/dark-green.jpg -fill yellow \
  -font /usr/share/fonts/truetype/inconsolata/Inconsolata.otf \
  -pointsize 32 -gravity SouthEast \
  -annotate 0 'OSiUX.com        ' \
  img/green-font.jpg
#+END_SRC

[[file:img/green-font.jpg][file:tmb/green-font.jpg]]

** dither + colorspace

#+BEGIN_EXAMPLE
  convert img/osiux-color.jpg +dither -colors 2 -colorspace gray -contrast-stretch 0 img/osiux-gray-2.jpg
  convert img/osiux-color.jpg +dither -colors 4 -colorspace gray -contrast-stretch 0 img/osiux-gray-4.jpg
  convert img/osiux-color.jpg +dither -colors 8 -colorspace gray -contrast-stretch 0 img/osiux-gray-8.jpg
  convert -append img/osiux-gray-2.jpg img/osiux-gray-4.jpg img/osiux-gray-8.jpg img/osiux-gray.jpg
#+END_EXAMPLE

[[file:img/osiux-gray.jpg][file:tmb/osiux-gray.jpg]]

** edge

#+BEGIN_SRC sh :session :results none :exports code
  convert img/osiux-color.jpg -colorspace Gray -edge 1 img/osiux-edge-1.jpg
  convert img/osiux-color.jpg -colorspace Gray -edge 2 img/osiux-edge-2.jpg
  convert img/osiux-color.jpg -colorspace Gray -edge 3 img/osiux-edge-3.jpg
  convert -append img/osiux-edge-1.jpg img/osiux-edge-2.jpg img/osiux-edge-3.jpg img/osiux-edge.jpg
#+END_SRC

[[file:img/osiux-edge.jpg][file:tmb/osiux-edge.jpg]]

** edge + Negate

#+BEGIN_SRC sh :session :results none :exports code
  convert img/osiux-color.jpg -colorspace Gray -edge 1 -negate img/osiux-edge-negate-1.jpg
  convert img/osiux-color.jpg -colorspace Gray -edge 2 -negate img/osiux-edge-negate-2.jpg
  convert img/osiux-color.jpg -colorspace Gray -edge 3 -negate img/osiux-edge-negate-3.jpg
  convert -append img/osiux-edge-negate-1.jpg img/osiux-edge-negate-2.jpg img/osiux-edge-negate-3.jpg img/osiux-edge-negate.jpg
#+END_SRC

[[file:img/osiux-edge-negate.jpg][file:tmb/osiux-edge-negate.jpg]]

** edge + negate + blur

#+BEGIN_SRC sh :session :results none :exports code
  for i in {1..3};do convert img/osiux-edge-negate-$i.jpg -blur 0x1 img/osiux-edge-negate-blur-$i.jpg;done
  convert -append img/osiux-edge-negate-blur-1.jpg img/osiux-edge-negate-blur-2.jpg img/osiux-edge-negate-blur-3.jpg img/osiux-edge-negate-blur.jpg
#+END_SRC

[[file:img/osiux-edge-negate-blur.jpg][file:tmb/osiux-edge-negate-blur.jpg]]

** charcoal (edge + negate + blur)

#+BEGIN_SRC sh :session :results none :exports code
  for i in {1..3};do convert img/osiux-color.jpg -charcoal $i img/osiux-charcoal-$i.jpg;done
  convert -append img/osiux-charcoal-1.jpg img/osiux-charcoal-2.jpg img/osiux-charcoal-3.jpg img/osiux-charcoal.jpg
#+END_SRC

[[file:img/osiux-charcoal.jpg][file:tmb/osiux-charcoal.jpg]]

** append

#+BEGIN_SRC sh :session :results none :exports code
  convert +append img/osiux-gray.jpg img/osiux-edge.jpg img/osiux-edge-negate.jpg img/osiux-charcoal.jpg img/osiux-edge-negate-blur.jpg img/osiux-5x3.jpg
  composite -compose colorburn -gravity Center img/osiux-5x3.jpg img/green-font.jpg img/osiux-image-magick.jpg
#+END_SRC

[[file:img/osiux-image-magick.jpg][file:tmb/osiux-image-magick.jpg]]
