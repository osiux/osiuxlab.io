#+TITLE:       Software Libre y Conectar Igualdad
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2011-10-24 22:19
#+HTML_HEAD:   <meta property="og:title" content="Software Libre y Conectar Igualdad" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2011-10-24" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2011-10-24-software-libre-en-monte-caseros-y-ubuntu-11.10-en-conectar-igualdad.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/conectar-igualdad/2011-10-22_ubuntu-conectar-igualdad_10.jpg" />


[[file:img/conectar-igualdad/2011-10-22_ubuntu-conectar-igualdad_10.jpg]]
[[file:img/conectar-igualdad/2011-10-22_ubuntu-conectar-igualdad_14.jpg]]

El viernes estuve en Monte Caseros y el sábado en Curuzú Cuatiá, el
viaje tuvo buenos resultados, en primer lugar dí un par de charlas
sobre Software Libre, Cooperativismo, Redes Libres y en segundo lugar
instalé un Ubuntu 11.10 en las notebooks de Conectar Igualdad,
comparto dos notas sobre cada día y espero que le sea útil a más de
uno.

- [[file:olimpiadas-informaticas-en-monte-caseros.org][Software Libre en las Olimpíadas Informáticas de Monte Caseros]]
- [[file:ubuntu-11.10-en-exomate-de-conectar-igualdad.org][Instalando Ubuntu 11.10 en Exomate de Conectar Igualdad]]

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/d29f58f68b1946e5e22d394744efa4a5c22e485a][=2013-06-19 12:55=]] @ 01:00 hs - Convierto notas en rST a Org y agrego últimas modificaciones sin commitear.
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
